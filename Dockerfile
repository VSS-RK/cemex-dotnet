FROM microsoft/aspnetcore:latest
RUN apt-get -y update && \
    apt-get -y install \
        nodejs \
        git && \
    apt-get clean && \
    npm set progress=false && \
    npm i -g cypress-cli && \
    cypress install && \
    npm cache clean